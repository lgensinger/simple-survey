module.exports = {
    devServer: {
        disableHostCheck: true,
        proxy: {
            "^/ocai": {
                target: `${process.env.VUE_APP_API_OCAI_PROXY_URL}`
            }
        }
    },
    publicPath: process.env.NODE_ENV === "production" && process.env.DEPLOY_TO_PAGES ? "/simple-survey/" : "/"
}
