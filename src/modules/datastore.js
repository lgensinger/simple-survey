import axios from "axios";
import { configuration, configurationRoutes } from "../configuration.js";

/**
 * Datastore is a an API access abstration.
 * @param {string} service - API base service url partial
 */
class Datastore {
    constructor(service) {
        this.service = configurationRoutes.BASE;
    }

    /**
     * Execute a query against API.
     * @param {object} data - key/value pairs
     * @returns A Promise resolving to an array of objects.
     */
    push(data) {

        // construct payload
        let payload = {
            data: data
        };

        // hit API
        return axios.post(`${this.service}/submissions`, payload, configuration.POSTCONFIG)
            .then(response => response)
            .catch(error => error);

    }

}

export { Datastore };
export default Datastore;
