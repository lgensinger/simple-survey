const configuration = {
    POSTCONFIG: { headers: {"Content-Type": "application/json"} }
};

const configurationRoutes = {
    BASE: process.env.VUE_APP_SERVICE_BASE ? process.env.VUE_APP_SERVICE_BASE : "/"
}

export { configuration, configurationRoutes };
export default configuration;
